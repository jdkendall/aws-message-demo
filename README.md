# Message Processing Demo

## Codebase Layout
[iac/](iac/) folder is for the CloudFormation templates and other Infrastructure as Code (IaC) files. 

 - [iac/resources.yaml](iac/resources.yaml): This CloudFormation template defines the AWS resources used in this application.

The [src/](src/) folder contains the source code for the Lambda functions.

 - [processMessage.js](src/processMessage.js): Lambda function to process incoming messages and log them to DynamoDB.
 - [dispatchMessage.js](src/dispatchMessage.js): Lambda function to update messages in DynamoDB and dispatch them via SNS.

The [doc/](doc/) folder contains an example Postman API call and some helpful diagrams.

## Deployment

### Prerequisites

 - AWS CLI installed and configured
 - Node.js and npm installed

1. Package Dependencies

Navigate to your project folder and install Node.js dependencies if you haven't already:

```bash
cd <projectFolder>
npm install
```

2. Zip Packaging

Package your Node.js application. (The resulting .zip file will be placed in a `build/` folder.)

```bash
npm run package
```

3. Upload to CloudFormation

 - Open the AWS CloudFormation console.
 - Choose 'Create Stack' and then upload your CloudFormation YAML file.
 - Follow the on-screen instructions to create the stack, updating parameters with your own unique values for, e.g., lambda code bucket name.

4. Upload Code to S3

 - Open the AWS S3 console.
 - Open the S3 bucket you configured for the lambda code bucket in step 3.
 - Upload the packaged .zip to the S3 bucket.
 - Note the URL from the .zip file for use in later steps.

5. Deploy Code to Lambdas via S3:

Do the following for both lambda functions, DispatchMessage & ProcessMessage:

 - Open the AWS Lambda console.
 - Choose the Lambda function you want to update.
 - Under 'Function code', choose 'Upload a file from Amazon S3', then input the S3 URL of your packaged .zip from step 4.
 - Click on 'Save' to update the Lambda function with the new code.

6. Deploy API Gateway to Stage:

 - Open the AWS API Gateway console.
 - Choose your API.
 - Go to the 'Deployments' section in the left sidebar.
 - Click on 'Create' to make a new deployment or choose an existing stage to deploy to.

 7. Generate an API Key in API Gateway:

 - Open the AWS API Gateway console.
 - Choose your API.
 - Go to the 'API Keys' section in the left sidebar.
 - Click on 'Create API Key'.
 - Provide a name and description, then click 'Save'.
 - Attach the newly created API Key to your usage plan or directly to your deployed API stage.

## Running

Once deployed, you can configure the [Postman example](doc/Example.postman_collection.json) with environment variables that point to the [Invoke Url for your stage from step 7](doc/API%20Gateway%20Staging%20Example.png).

Remember to confirm the subscription for the email you set up as part of step 2, as SNS will not be able to deliver messages to the email until the subscription is confirmed.

SMS/text messages via SNS will also require additional setup by adding your phone number to the sandbox in the SNS topic. You can look up the SNS topic in the CloudFormation stack view or by visiting the SNS topic in the web console.

If deployed & configured successfully, on invoking the Postman call you should receive an email containing the details from the JSON body you passed in the call. If improperly configured, errors will be found in the CloudWatch Logs, which can be accessed through the Monitor tab on each of the Lambdas, or directly from the Log Groups subsection of CloudWatch.

## System definition

All resources are defined in the singular CloudFormation file [resources.yaml](iac/resources.yaml). These are defined such that manual deployment to Lambda is necessary, as well as the creation & deployment of an API Gateway stage -- this is an explicit decision for learning purposes, and in production systems these creation & deployments would either be handled by DevOps through CloudFormation or a tool such as the [Serverless Framework](https://serverless.com/).

Data flows through the system as depicted in [the data flow diagram](doc/Data%20Flow.png) and the overall structure of the system is depicted in the [structure diagram](doc/Basic%20Structure.png). A brief visual mapping for each of the lambda functions to its respective file can be found [here](doc/Lambda%20Handler%20Target%20Mapping.png).