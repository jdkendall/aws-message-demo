// dispatchMessage.js

const AWS = require('aws-sdk');
const sns = new AWS.SNS();

exports.handler = async (event) => {
    let statusCode = 200;
    let responseBody = '';

    // Loop through each message in the SQS batch
    for (const record of event.Records) {
        const { message, username, timestamp } = JSON.parse(record.body);

        const snsParams = {
            Message: `Timestamp: ${timestamp}, Username: ${username}, Message: ${message}`,
            TopicArn: process.env['snsTopicArn'],
        };

        const dynamoDbParams = {
            TableName: process.env['dynamoDbTable'],
            Key: {
                timestamp,
                username
            },
            UpdateExpression: 'set dispatched = :val',
            ExpressionAttributeValues: {
                ':val': true
            },
            ReturnValues: 'UPDATED_NEW'
        };

        try {
            // Publish to our SNS subscribers (email, phone, etc)
            await sns.publish(snsParams).promise();
            
            // Update our DynamoDB record for the message that it has been dispatched
            await dynamoDb.update(dynamoDbParams).promise();
            
            responseBody += `Successfully published message: ${message}\n`;
        } catch (err) {
            statusCode = 500;
            responseBody += `Failed to publish message: ${err}\n`;
        }
    }

    return {
        statusCode,
        body: JSON.stringify({ result: responseBody }),
    };
};
