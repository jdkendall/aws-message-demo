// processMessage.js

const AWS = require('aws-sdk');

const sqs = new AWS.SQS();
const dynamoDb = new AWS.DynamoDB.DocumentClient();

exports.handler = async (event) => {
    let statusCode = 200;
    let responseBody = '';

    const { message, username, timestamp } = JSON.parse(event.body);

    if (!message || !username || !timestamp) {
        return {
            statusCode: 400,
            body: JSON.stringify({ error: 'Missing parameters' }),
        };
    }

    const sqsParams = {
        MessageBody: JSON.stringify({ message, username, timestamp }),
        QueueUrl: process.env['queueUrl'],
    };

    const dynamoDbParams = {
        TableName: process.env['dynamoDbTable'],
        Item: {
            timestamp,
            username,
            message
        }
    };

    try {
        // Queue up our message for dispatch
        await sqs.sendMessage(sqsParams).promise();

        // Log the message to DynamoDB
        await dynamoDb.put(dynamoDbParams).promise();

        responseBody = { result: "Message sent to SQS queue & logged to DynamoDB" };
    } catch (err) {
        statusCode = 500;
        responseBody = { result: `Failed to send message: ${err}` };
    }

    return {
        statusCode,
        body: JSON.stringify(responseBody),
    };
};

